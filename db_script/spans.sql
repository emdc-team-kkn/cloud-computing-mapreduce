--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: spans; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE spans (
    id integer NOT NULL,
    tower_id character varying(255),
    maxspan integer,
    date date
);


--
-- Name: spans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE spans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: spans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE spans_id_seq OWNED BY spans.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY spans ALTER COLUMN id SET DEFAULT nextval('spans_id_seq'::regclass);


--
-- Name: spans_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY spans
    ADD CONSTRAINT spans_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

