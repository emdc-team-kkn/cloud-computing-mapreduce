package pt.ulisboa.tecnico.kkn.output;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.lib.db.DBWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.*;


/**
 * The output class of the BirdLastRecordTool
 */
public class BirdLastRecordOutput implements Writable, DBWritable {

    private String birdTag;
    private Timestamp lastSeen;

    /**
     * Default constructor, doesn't do anything but create the object
     */
    public BirdLastRecordOutput() {

    }

    public BirdLastRecordOutput(String birdTag, Timestamp lastSeen) {
        this.birdTag = birdTag;
        this.lastSeen = lastSeen;
    }

    @Override
    public void write(PreparedStatement statement) throws SQLException {
        statement.setString(1, birdTag);
        statement.setTimestamp(2, lastSeen);
    }

    @Override
    public void readFields(ResultSet resultSet) throws SQLException {
        birdTag = resultSet.getString("bird_tag");
        lastSeen = resultSet.getTimestamp("last_seen");
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(birdTag);
        out.writeLong(lastSeen.getTime());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        birdTag = in.readUTF();
        long timestamp = in.readLong();
        lastSeen = new Timestamp(timestamp);
    }

    public String getBirdTag() {
        return birdTag;
    }

    public void setBirdTag(String birdTag) {
        this.birdTag = birdTag;
    }

    public Timestamp getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Timestamp lastSeen) {
        this.lastSeen = lastSeen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BirdLastRecordOutput that = (BirdLastRecordOutput) o;

        if (!birdTag.equals(that.birdTag)) return false;
        if (!lastSeen.equals(that.lastSeen)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = birdTag.hashCode();
        result = 31 * result + lastSeen.hashCode();
        return result;
    }
}
