package pt.ulisboa.tecnico.kkn.output;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.lib.db.DBWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * The output class of the BirdWeightTool
 */
public class BirdWeightOutput implements Writable, DBWritable {

    private String towerId;
    private Double weight;
    private Timestamp date;

    /**
     * Default constructor, doesn't do anything but create the object
     */
    public BirdWeightOutput() {

    }

    public BirdWeightOutput(String towerId, Timestamp date, Double weight) {
        this.towerId = towerId;
        this.date = date;
        this.weight = weight;
    }

    @Override
    public void write(PreparedStatement statement) throws SQLException {
        statement.setString(1, towerId);
        statement.setDouble(2, weight);
        statement.setTimestamp(3, date);
    }

    @Override
    public void readFields(ResultSet resultSet) throws SQLException {
        towerId = resultSet.getString("tower_id");
        weight = resultSet.getDouble("weight");
        date = resultSet.getTimestamp("date");
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(towerId);
        out.writeDouble(weight);
        out.writeLong(date.getTime());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.towerId = in.readUTF();
        this.weight = in.readDouble();
        long timestamp = in.readLong();
        this.date = new Timestamp(timestamp);
    }

    public String getTowerId() {
        return towerId;
    }

    public Double getWeight() {
        return weight;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setTowerId(String towerId) {
        this.towerId = towerId;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BirdWeightOutput that = (BirdWeightOutput) o;

        if (!weight.equals(that.weight)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "BirdWeightOutput{" +
                "towerId='" + towerId + '\'' +
                ", weight=" + weight +
                ", date=" + date +
                '}';
    }

    @Override
    public int hashCode() {
        int result = towerId.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }
}