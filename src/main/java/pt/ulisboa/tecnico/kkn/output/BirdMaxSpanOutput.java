package pt.ulisboa.tecnico.kkn.output;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.lib.db.DBWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * The output class of the BirdMaxSpanTool
 */

public class BirdMaxSpanOutput implements Writable, DBWritable {

    private String towerId;
    private Integer maxspan;
    private Timestamp date;

    /**
     * Default constructor, doesn't do anything but create the object
     */
    public BirdMaxSpanOutput() {

    }

    public BirdMaxSpanOutput(String towerId, Integer maxspan, Timestamp date) {
        this.towerId = towerId;
        this.date = date;
        this.maxspan = maxspan;
    }

    @Override
    public void write(PreparedStatement statement) throws SQLException {
        statement.setString(1, towerId);
        statement.setInt(2, maxspan);
        statement.setTimestamp(3, date);
    }

    @Override
    public void readFields(ResultSet resultSet) throws SQLException {
        towerId = resultSet.getString("tower_id");
        maxspan = resultSet.getInt("maxspan");
        date = resultSet.getTimestamp("date");
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(towerId);
        out.writeInt(maxspan);
        out.writeLong(date.getTime());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.towerId = in.readUTF();
        this.maxspan = in.readInt();
        long timestamp = in.readLong();
        this.date = new Timestamp(timestamp);
    }

    public String getTowerId() {
        return towerId;
    }

    public Integer getMaxSpan() {
        return maxspan;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setTowerId(String towerId) {
        this.towerId = towerId;
    }

    public void setMaxSpan(Integer max_span) {
        this.maxspan = max_span;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BirdMaxSpanOutput that = (BirdMaxSpanOutput) o;

        if (!maxspan.equals(that.maxspan)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "BirdMaxSpanOutput{" +
                "towerId='" + towerId + '\'' +
                ", maxspan=" + maxspan +
                ", date=" + date +
                '}';
    }

    @Override
    public int hashCode() {
        int result = towerId.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }
}