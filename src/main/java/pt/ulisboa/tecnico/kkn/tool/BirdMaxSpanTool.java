package pt.ulisboa.tecnico.kkn.tool;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.db.DBConfiguration;
import org.apache.hadoop.mapreduce.lib.db.DBOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import pt.ulisboa.tecnico.kkn.output.BirdMaxSpanOutput;
import pt.ulisboa.tecnico.kkn.utils.DBConfig;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
* This class produces a log. Each row of this log consists of date, tower id and maximum span of the bird observed
* during rain conditions at this date.
*  
*/

public class BirdMaxSpanTool extends Configured implements Tool {

    private final static Log log = LogFactory.getLog(BirdMaxSpanTool.class);
    //need to update
    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        FileSystem fs = FileSystem.get(conf);

        DBConfig dbConfig = DBConfig.getInstance();
        DBConfiguration.configureDB(conf,
                "org.postgresql.Driver",
                dbConfig.getUri(),
                dbConfig.getUsername(),
                dbConfig.getPassword()
        );

        Job job = Job.getInstance(conf, "bird-maxspan");
        job.setJarByClass(BirdMaxSpanTool.class);

        //Mapper
        job.setMapperClass(MaxSpanMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //Combiner
        job.setCombinerClass(MaxSpanCombiner.class);

        //Reducer
        job.setReducerClass(MaxSpanReducer.class);
        job.setOutputKeyClass(BirdMaxSpanOutput.class);
        job.setOutputValueClass(Object.class);

        //Output format
        job.setOutputFormatClass(DBOutputFormat.class);

        DBOutputFormat.setOutput(job, "spans", "tower_id", "maxspan", "date"); 

        //Setup input path
        Path inputPath = new Path(args[0]);
        FileStatus[] statuses = fs.listStatus(inputPath);
        for(FileStatus f: statuses) {
            FileInputFormat.addInputPath(job, f.getPath());
        }

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static class MaxSpanMapper extends Mapper<Object, Text, Text, IntWritable>{

        private final static IntWritable span = new IntWritable();
        private Text towerDate = new Text();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String line;
            String[] tokens;

            //Log format: Mafra-0, 2014-01-22, 10:35:02, swallow-19, 5, 123, 1
            //Write: <key. value> = <tower-id;date, weight>
            line = value.toString();
            tokens = line.split(", ");

            if (tokens[3].equals("-1") || !tokens[6].equals("2")){
                return;
            }


            //set the span of the bird
            span.set(Integer.parseInt(tokens[5]));
            //set the key = tower-id;date
            towerDate.set(tokens[0] + ";" + tokens[1]);

            context.write(towerDate, span);
        }
    }


    public static class MaxSpanReducer extends Reducer<Text, IntWritable, BirdMaxSpanOutput, Writable> {

        private BirdMaxSpanOutput result = new BirdMaxSpanOutput(); 
        private SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            //Initialize variable to find a maximum span
            Integer max_span = -1;
            Integer span = 0;

            //Parse tower id and date from key
            String keys = key.toString();
            String[] keyTokens = keys.split(";");
            String towerId = keyTokens[0];

            for (IntWritable val : values) {
                span = val.get();
                if (span > max_span) {
                	max_span = span;
                }
            }

            Date date = null;
            try {
                date = dateParser.parse(keyTokens[1]);
                Timestamp time = new Timestamp(date.getTime());

                //set result
                result.setTowerId(towerId);
                result.setDate(time);
                result.setMaxSpan(max_span);
                context.write(result, NullWritable.get());
            } catch (ParseException e) {
                log.warn("Date parse error", e);
            }
        }
    }


    public static class MaxSpanCombiner extends Reducer<Text,IntWritable,Text,IntWritable> {

        private IntWritable result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            //Initialize weight sum
            Integer max_span = -1;
            Integer span = 0;

            for (IntWritable val : values) {
                span = val.get();
                if(span > max_span) {
                    max_span = span;
                }
            }
            //set combiner result
            result.set(max_span);

            context.write(key, result);
        }
    }

}