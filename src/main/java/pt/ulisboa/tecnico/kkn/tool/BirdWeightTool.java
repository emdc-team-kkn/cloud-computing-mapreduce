package pt.ulisboa.tecnico.kkn.tool;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.db.DBConfiguration;
import org.apache.hadoop.mapreduce.lib.db.DBOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import pt.ulisboa.tecnico.kkn.output.BirdWeightOutput;
import pt.ulisboa.tecnico.kkn.utils.DBConfig;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
* This class produces a log that summarizes the weight of the birds
* logged by a certain tower on a certain date
* */
public class BirdWeightTool extends Configured implements Tool {

    private final static Log log = LogFactory.getLog(BirdWeightTool.class);
    //need to update
    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        FileSystem fs = FileSystem.get(conf);

        DBConfig dbConfig = DBConfig.getInstance();
        DBConfiguration.configureDB(conf,
                "org.postgresql.Driver",
                dbConfig.getUri(),
                dbConfig.getUsername(),
                dbConfig.getPassword()
        );

        Job job = Job.getInstance(conf, "bird-weight");
        job.setJarByClass(BirdWeightTool.class);

        //Mapper
        job.setMapperClass(WeightMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(DoubleWritable.class);

        //Combiner
        job.setCombinerClass(WeightSumCombiner.class);

        //Reducer
        job.setReducerClass(WeightSumReducer.class);
        job.setOutputKeyClass(BirdWeightOutput.class);
        job.setOutputValueClass(Object.class);

        //Output format
        job.setOutputFormatClass(DBOutputFormat.class);
        DBOutputFormat.setOutput(job, "weights", "tower_id", "weight", "date");

        //Setup input path
        Path inputPath = new Path(args[0]);
        FileStatus[] statuses = fs.listStatus(inputPath);
        for(FileStatus f: statuses) {
            FileInputFormat.addInputPath(job, f.getPath());
        }

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static class WeightMapper extends Mapper<Object, Text, Text, DoubleWritable>{

        private final static DoubleWritable weight = new DoubleWritable();
        private Text towerDate = new Text();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String line;
            String[] tokens;

            //Log format: Mafra-0, 2014-01-22, 10:35:02, swallow-19, 5, 123, 1
            //Write: <key. value> = <tower-id;date, weight>
            line = value.toString();
            tokens = line.split(", ");

            if (tokens[3].equals("-1")){
                return;
            }

            //set the weight of the bird
            weight.set(Long.valueOf(tokens[4]));
            //set the key = tower-id;date
            towerDate.set(tokens[0] + ";" + tokens[1]);

            context.write(towerDate, weight);
        }
    }

    public static class WeightSumReducer extends Reducer<Text,DoubleWritable,BirdWeightOutput,Writable> {
        private BirdWeightOutput result = new BirdWeightOutput();
        private SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");

        public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            //Initialize weight sum
            Double sum = 0.0;

            //Parse tower id and date from key
            String keys = key.toString();
            String[] keyTokens = keys.split(";");
            String towerId = keyTokens[0];

            for (DoubleWritable val : values) {
                sum += val.get();
            }

            Date date = null;
            try {
                date = dateParser.parse(keyTokens[1]);
                Timestamp time = new Timestamp(date.getTime());

                //set result
                result.setTowerId(towerId);
                result.setDate(time);
                result.setWeight(sum);

                context.write(result, NullWritable.get());
            } catch (ParseException e) {
                log.warn("Date parse error", e);
            }
        }
    }

    public static class WeightSumCombiner extends Reducer<Text,DoubleWritable,Text,DoubleWritable> {
        private DoubleWritable result = new DoubleWritable();

        public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            //Initialize weight sum
            Double sum = 0.0;

            for (DoubleWritable val : values) {
                sum += val.get();
            }
            //set combiner result
            result.set(sum);

            context.write(key, result);
        }
    }
}
