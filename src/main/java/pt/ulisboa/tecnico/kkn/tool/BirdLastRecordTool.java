package pt.ulisboa.tecnico.kkn.tool;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.db.DBConfiguration;
import org.apache.hadoop.mapreduce.lib.db.DBOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import pt.ulisboa.tecnico.kkn.output.BirdLastRecordOutput;
import pt.ulisboa.tecnico.kkn.utils.DBConfig;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * This class process log to find out the last appearance of a tagged bird.
 * We can use that information to query what tagged birds have not appeared in one week
 */
public class BirdLastRecordTool extends Configured implements Tool{

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        FileSystem fs = FileSystem.get(conf);

        DBConfig dbConfig = DBConfig.getInstance();
        DBConfiguration.configureDB(conf,
                "org.postgresql.Driver",
                dbConfig.getUri(),
                dbConfig.getUsername(),
                dbConfig.getPassword()
        );

        Job job = Job.getInstance(conf, "last-seen-bird");
        job.setJarByClass(BirdLastRecordTool.class);

        //Mapper
        job.setMapperClass(LastRecordMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        //Combiner
        job.setCombinerClass(LastRecordCombiner.class);

        //Reducer
        job.setReducerClass(LastRecordReducer.class);
        job.setOutputKeyClass(BirdLastRecordOutput.class);
        job.setOutputValueClass(Object.class);

        //Output format
        job.setOutputFormatClass(DBOutputFormat.class);
        DBOutputFormat.setOutput(job, "appearances", "bird_tag", "last_seen");

        //Setup input path
        Path inputPath = new Path(args[0]);
        FileStatus[] statuses = fs.listStatus(inputPath);
        for(FileStatus f: statuses) {
            FileInputFormat.addInputPath(job, f.getPath());
        }

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static class LastRecordMapper extends Mapper<Object, Text, Text, LongWritable> {
        private LongWritable valueOut = new LongWritable();
        private Text keyOut = new Text();
        private SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        private static final Log log = LogFactory.getLog(LastRecordMapper.class);

        @Override
        public void map(Object key, Text value, Context ctx) throws IOException, InterruptedException {
            String line = value.toString();
            String[] tokens = line.split("\\s*,\\s*");

            //Malformed input line, we ignore this
            if(tokens.length < 7) {
                return;
            }

            //Ignore untagged bird and weather-only data
            if (tokens[3].equals("-1") || tokens[3].equals("0")) {
                return;
            }

            try {
                //Set the key to the tagged bird
                keyOut.set(tokens[3]);

                //Set the value to time
                Date date = dateParser.parse(tokens[1] + " " + tokens[2]);
                valueOut.set(date.getTime());

                ctx.write(keyOut, valueOut);
            } catch (ParseException e) {
                //Ignore parse exception, probably malformed input
                log.warn("Unable to parse time value", e);
            }
        }
    }

    public static class LastRecordReducer extends Reducer<Text, LongWritable, BirdLastRecordOutput, Writable> {
        BirdLastRecordOutput out = new BirdLastRecordOutput();

        @Override
        public void reduce(Text key, Iterable<LongWritable> value, Context ctx)
                throws IOException, InterruptedException {
            long lastDate = -1;
            Iterator<LongWritable> it = value.iterator();
            long tmp;
            while (it.hasNext()) {
                tmp = it.next().get();
                if (tmp > lastDate) {
                    lastDate = tmp;
                }
            }
            out.setBirdTag(key.toString());
            out.setLastSeen(new Timestamp(lastDate));
            ctx.write(out, NullWritable.get());
        }
    }

    public static class LastRecordCombiner extends Reducer<Text, LongWritable, Text, LongWritable> {
        private LongWritable valueOut = new LongWritable();

        @Override
        public void reduce(Text key, Iterable<LongWritable> value, Context ctx)
                throws IOException, InterruptedException {
            long lastDate = 0;
            Iterator<LongWritable> it = value.iterator();
            long tmp;
            while (it.hasNext()) {
                tmp = it.next().get();
                if (tmp > lastDate) {
                    lastDate = tmp;
                }
            }
            valueOut.set(lastDate);

            ctx.write(key, valueOut);
        }
    }
}
