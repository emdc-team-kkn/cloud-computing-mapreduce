package pt.ulisboa.tecnico.kkn.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class is used to read the database configuration file.
 */
public class DBConfig {

    private static DBConfig instance = null;
    private Properties properties;
    private final static Log log = LogFactory.getLog(DBConfig.class);

    private DBConfig() {}

    public static DBConfig getInstance() throws IOException {
        if (instance == null) {
            instance = new DBConfig();
            instance.properties = new Properties();
            InputStream dbConfigFile =
                    Thread.currentThread().getContextClassLoader().getResourceAsStream("db.properties");
            try {
                instance.properties.load(dbConfigFile);
            } catch (IOException e) {
                log.trace("Unable to read database configuration", e);
                throw e;
            } finally {
                //We try to close the configuration file
                dbConfigFile.close();
            }
        }
        return instance;
    }

    public String getUri() {
        return properties.getProperty("uri");
    }

    public String getUsername() {
        return properties.getProperty("username");
    }

    public String getPassword() {
        return properties.getProperty("password");
    }
}
