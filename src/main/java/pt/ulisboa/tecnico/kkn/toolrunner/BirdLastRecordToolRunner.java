package pt.ulisboa.tecnico.kkn.toolrunner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;
import pt.ulisboa.tecnico.kkn.tool.BirdLastRecordTool;

/**
 * This class is used to run the BirdLastRecordTool
 */
public class BirdLastRecordToolRunner {

    public static void main(final String[] args) throws Exception {
        Configuration conf = new Configuration();
        int res = ToolRunner.run(conf, new BirdLastRecordTool(), args);
        System.exit(res);
    }

}
