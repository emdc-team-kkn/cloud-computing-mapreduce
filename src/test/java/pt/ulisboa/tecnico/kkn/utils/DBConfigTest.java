package pt.ulisboa.tecnico.kkn.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Test class for DBConfig
 */
public class DBConfigTest {

    @Test
    public void testDBConfig() throws IOException {
        DBConfig config = DBConfig.getInstance();
        Assert.assertEquals(config.getUri(), "jdbc:postgresql://localhost/bird_logs");
        Assert.assertEquals(config.getUsername(), "testuser");
        Assert.assertEquals(config.getPassword(), "testpassword");
    }
}
