package pt.ulisboa.tecnico.kkn.tool;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pt.ulisboa.tecnico.kkn.output.BirdWeightOutput;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by nikola on 27.10.14.
 */
public class BirdWeightToolTest {
    private MapDriver<Object, Text, Text, DoubleWritable> mapDriver;
    private ReduceDriver<Text, DoubleWritable, BirdWeightOutput, Writable> reduceDriver;
    private MapReduceDriver<Object, Text, Text, DoubleWritable, BirdWeightOutput, Writable> mpDriver;

    @BeforeClass
    public void SetUp() {
        BirdWeightTool.WeightMapper weightMapper = new BirdWeightTool.WeightMapper();
        BirdWeightTool.WeightSumCombiner weightSumCombiner = new BirdWeightTool.WeightSumCombiner();
        BirdWeightTool.WeightSumReducer weightSumReducer = new BirdWeightTool.WeightSumReducer();


        mapDriver = MapDriver.newMapDriver(weightMapper);
        reduceDriver = ReduceDriver.newReduceDriver(weightSumReducer);

        mpDriver = MapReduceDriver.newMapReduceDriver(weightMapper, weightSumReducer, weightSumCombiner);
    }
    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(NullWritable.get(), new Text("Lisboa-8, 2014-08-07, 15:33:39, falcon-35, 3, 206, 2"));
        mapDriver.withOutput(new Text("Lisboa-8;2014-08-07"), new DoubleWritable(Double.valueOf("3")));
        mapDriver.runTest();
    }

    @Test
    public void testReducer() throws ParseException, IOException {
        reduceDriver.withInput(new Text("Lisboa-8;2014-08-07"), Arrays.asList(
                new DoubleWritable(Double.valueOf("3")),
                new DoubleWritable(Double.valueOf("4")),
                new DoubleWritable(Double.valueOf("5")),
                new DoubleWritable(Double.valueOf("6"))
        ));
        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateParser.parse("2014-08-07");
        reduceDriver.withOutput(new BirdWeightOutput("Lisboa-8", new Timestamp(date.getTime()), new Double(18.0)), NullWritable.get());

        reduceDriver.runTest();
    }

    @Test
    public void testMapReducer() throws ParseException, IOException {
        mpDriver.withInput(NullWritable.get(), new Text("Lisboa-8, 2014-08-07, 15:33:39, falcon-35, 3, 206, 2"));
        mpDriver.withInput(NullWritable.get(), new Text("Cascais-7, 2014-08-07, 15:33:39, falcon-35, 7, 206, 2"));
        mpDriver.withInput(NullWritable.get(), new Text("Sintra-9, 2014-07-07, 15:33:39, falcon-35, 6, 206, 2"));
        mpDriver.withInput(NullWritable.get(), new Text("Sintra-9, 2014-07-07, 15:33:39, falcon-35, 14, 206, 2"));

        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateParser.parse("2014-08-07");
        mpDriver.withOutput(new BirdWeightOutput("Lisboa-8", new Timestamp(date.getTime()), new Double(3.0)), NullWritable.get());

        mpDriver.withOutput(new BirdWeightOutput("Cascais-7", new Timestamp(date.getTime()), new Double(7.0)), NullWritable.get());

        date = dateParser.parse("2014-07-07");
        mpDriver.withOutput(new BirdWeightOutput("Sintra-9", new Timestamp(date.getTime()), new Double(20.0)), NullWritable.get());

        mpDriver.runTest(false);
    }
}
