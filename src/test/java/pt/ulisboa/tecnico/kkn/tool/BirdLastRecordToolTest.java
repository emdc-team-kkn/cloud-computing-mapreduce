package pt.ulisboa.tecnico.kkn.tool;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pt.ulisboa.tecnico.kkn.output.BirdLastRecordOutput;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Test class for BirdLastRecordTool
 */
public class BirdLastRecordToolTest {

    private MapDriver<Object, Text, Text, LongWritable> mapDriver;
    private ReduceDriver<Text, LongWritable, BirdLastRecordOutput, Writable> reduceDriver;
    private MapReduceDriver<Object, Text, Text, LongWritable, BirdLastRecordOutput, Writable> mrDriver;

    @BeforeClass
    public void setUp() {
        BirdLastRecordTool.LastRecordMapper mapper =
                new BirdLastRecordTool.LastRecordMapper();
        BirdLastRecordTool.LastRecordReducer reducer =
                new BirdLastRecordTool.LastRecordReducer();
        BirdLastRecordTool.LastRecordCombiner combiner = new BirdLastRecordTool.LastRecordCombiner();

        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mrDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer, combiner);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(
                NullWritable.get(),
                new Text("Cascais-4, 2013-07-23, 03:07:58, seagulf-44, 10, 177, 3")
        );
        mapDriver.withOutput(new Text("seagulf-44"), new LongWritable(1374545278000L));
        mapDriver.runTest();
    }

    @Test
    public void testReducer() throws IOException {
        reduceDriver.withInput(new Text("seagulf-44"),
                Arrays.asList(
                        new LongWritable(1374545281000L),
                        new LongWritable(1374545278000L),
                        new LongWritable(1374545280000L),
                        new LongWritable(1374545279000L)
                )
        );
        BirdLastRecordOutput expected =
                new BirdLastRecordOutput("seagulf-44", new Timestamp(1374545281000L));
        reduceDriver.withOutput(expected, NullWritable.get());
        reduceDriver.runTest();
    }

    @Test
    public void testBoth() throws IOException {
        mrDriver.withInput(
                NullWritable.get(),
                new Text("Cascais-4, 2012-07-23, 03:07:58, seagulf-44, 10, 177, 3")
        );
        mrDriver.withInput(
                NullWritable.get(),
                new Text("Cascais-4, 2013-06-23, 03:07:58, seagulf-42, 10, 177, 3")
        );
        mrDriver.withInput(
                NullWritable.get(),
                new Text("Cascais-4, 2013-07-22, 03:07:58, seagulf-44, 10, 177, 3")
        );
        mrDriver.withInput(
                NullWritable.get(),
                new Text("Cascais-4, 2013-07-23, 03:08:02, seagulf-44, 10, 177, 3")
        );
        BirdLastRecordOutput expected1 =
                new BirdLastRecordOutput("seagulf-44", new Timestamp(1374545282000L));
        BirdLastRecordOutput expected2 =
                new BirdLastRecordOutput("seagulf-42", new Timestamp(1371953278000L));
        mrDriver.withOutput(expected1, NullWritable.get());
        mrDriver.withOutput(expected2, NullWritable.get());
        mrDriver.runTest(false);
    }
}
