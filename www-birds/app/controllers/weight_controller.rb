class WeightController < ApplicationController

	def index
		@towers = Weight.select(:tower_id).order(:tower_id).distinct
		if params[:date]
			date_param = Date.strptime(params[:date], '%d-%m-%Y')
			@stats = Weight.where(:date => date_param, :tower_id =>  params[:tower_id])
			respond_to do |format|
				format.html
				format.json { render json: @stats }
			end
		else
			respond_to do |format|
				format.html
				format.json { render json: [] }
			end
		end
	end

end
