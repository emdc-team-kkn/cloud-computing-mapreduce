class SpanController < ApplicationController

  def index
    if params[:date]
      date_param = Date.strptime(params[:date], '%d-%m-%Y')
      @info = Span.where(date: date_param)
                  .paginate(page: params[:page], per_page: 10)
      respond_to do |format|
        format.html
        format.json {
          render json: {
                     :current_page => @info.current_page,
                     :per_page => @info.per_page,
                     :total_entries => @info.total_entries,
                     :entries => @info
                 }
        }
      end
    else
      respond_to do |format|
        format.html
        format.json {
          render json: {
                     :current_page => 0,
                     :per_page => 0,
                     :total_entries => 0,
                     :entries => []
                 }
        }
      end
    end
  end

end
