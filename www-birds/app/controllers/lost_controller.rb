class LostController < ApplicationController

  def index
    @lost = Appearance.where("last_seen < ?", 1.week.ago)
                .order(:last_seen, :bird_tag)
                .paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html
      format.json {
        render json: {
                   :current_page => @lost.current_page,
                   :per_page => @lost.per_page,
                   :total_entries => @lost.total_entries,
                   :entries => @lost
               }
      }
    end
  end

end
