class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :gen_prime

  def is_prime ( p )
    return true  if p == 2
    return false if p <= 1 || p.even?
    (3 .. Math.sqrt(p)).step(2) do |i|
      return false  if p % i == 0
    end
    true
  end

  def is_mersenne_prime ( p )
    return true  if p == 2
    m_p = ( 1 << p ) - 1
    s = 4
    (p-2).times { s = (s ** 2 - 2) % m_p }
    s == 0
  end

  def gen_prime
    precision = 150   # maximum requested number of decimal places of 2 ** MP-1 #
    long_bits_width = precision / Math.log(2) * Math.log(10)
    upb_prime = (long_bits_width - 1).to_i / 2    # no unsigned #
    upb_count = 45      # find 45 mprimes if int was given enough bits #

    count = 0
    (2..upb_prime).each { |p|
      if is_prime(p) && is_mersenne_prime(p)
        count += 1
      end
      break if count >= upb_count
    }
  end

end
