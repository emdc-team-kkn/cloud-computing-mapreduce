# Install

* Install Ruby virtual machine. 

 http://rvm.io

And get the latest version of Ruby. Currently: ruby 2.1.3p242

* Execute 

```
 gem install bundle
 bundle
```

* Copy ./config/database.yml.default to ./config/database.yml and change credentials.
   User must be able to create databases.

* Execute

```
 rake db:create
 rake db:migrate
```

* In JAVA project, database name should point to 'birds_development'.
* Execute all MapReduce tasks:

```
hadoop jar build/libs/cloud-computing-mapreduce-fat.jar pt.ulisboa.tecnico.kkn.toolrunner.BirdWeightToolRunner birds/logs

hadoop jar build/libs/cloud-computing-mapreduce-fat.jar pt.ulisboa.tecnico.kkn.toolrunner.BirdMaxSpanToolRunner birds/logs

hadoop jar build/libs/cloud-computing-mapreduce-fat.jar pt.ulisboa.tecnico.kkn.toolrunner.BirdLastRecordToolRunner birds/logs
```

* Start rails server:

```
 rails s
```

* Check http://localhost:3000