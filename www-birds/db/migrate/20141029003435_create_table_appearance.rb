class CreateTableAppearance < ActiveRecord::Migration
  def change
    create_table :appearances do |t|
 			t.string :bird_tag
    	t.date	 :last_seen

    	t.timestamps
    end
  end
end
