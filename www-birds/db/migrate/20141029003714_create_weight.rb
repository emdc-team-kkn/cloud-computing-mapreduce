class CreateWeight < ActiveRecord::Migration
  def change
    create_table :weights do |t|
    	t.string 	:tower_id
    	t.float 	:weight
    	t.date	 	:date

 			t.timestamps
    end
  end
end
