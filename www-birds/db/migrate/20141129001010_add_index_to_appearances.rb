class AddIndexToAppearances < ActiveRecord::Migration
  def change
    add_index :appearances, :last_seen
  end
end
