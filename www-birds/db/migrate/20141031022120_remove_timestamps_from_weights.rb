class RemoveTimestampsFromWeights < ActiveRecord::Migration
  def change
    remove_column :weights, :created_at, :string
    remove_column :weights, :updated_at, :string
  end
end
