class RemoveTimestampsFromAppearances < ActiveRecord::Migration
  def change
    remove_column :appearances, :created_at, :string
    remove_column :appearances, :updated_at, :string
  end
end
