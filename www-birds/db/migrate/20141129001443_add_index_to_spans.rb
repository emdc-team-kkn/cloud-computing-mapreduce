class AddIndexToSpans < ActiveRecord::Migration
  def change
    add_index :spans, :date
  end
end
