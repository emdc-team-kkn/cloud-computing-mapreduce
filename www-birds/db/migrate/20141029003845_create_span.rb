class CreateSpan < ActiveRecord::Migration
  def change
    create_table :spans do |t|
    	t.string  :tower_id
      t.integer :maxspan
      t.date 		:date

      t.timestamps
    end
  end
end
