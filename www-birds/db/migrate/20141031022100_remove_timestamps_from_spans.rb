class RemoveTimestampsFromSpans < ActiveRecord::Migration
  def change
    remove_column :spans, :created_at, :string
    remove_column :spans, :updated_at, :string
  end
end
