class AddIndexToWeights < ActiveRecord::Migration
  def change
    add_index :weights, [:tower_id, :date]
  end
end
