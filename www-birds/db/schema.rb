# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141129001443) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appearances", force: true do |t|
    t.string "bird_tag"
    t.date   "last_seen"
  end

  add_index "appearances", ["last_seen"], name: "index_appearances_on_last_seen", using: :btree

  create_table "spans", force: true do |t|
    t.string  "tower_id"
    t.integer "maxspan"
    t.date    "date"
  end

  add_index "spans", ["date"], name: "index_spans_on_date", using: :btree

  create_table "weights", force: true do |t|
    t.string "tower_id"
    t.float  "weight"
    t.date   "date"
  end

  add_index "weights", ["tower_id", "date"], name: "index_weights_on_tower_id_and_date", using: :btree

end
