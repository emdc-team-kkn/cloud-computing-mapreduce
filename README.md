# Build system
This project uses the gradle build system.

Documents about gradle:

* [http://www.gradle.org/][1]

Gradle IDE integration:

* IntelliJ: Built-in integration in the latest version
* Eclipse: [Eclipse Plugin][2]

# How to build

    gradle build

or

    ./gradlew build

After build, the jar file will be in `build/libs`

# How to run test

    gradle test

or

    ./gradlew test

The test report will be in `build/reports/tests`

# How to write test
The test library is TestNG. Documents can be found at

* [Basic guide][3]
* [Complete reference][4]

# Database configuration
Copy `src/main/resources/db.properties.default` to `src/main/resources/db.properties` and edit it
with your machine database configuration

# Building jar file
Use the following command to build the fatjar containing all runtime dependencies

    gradle fatjar
    
or

    ./gradlew fatjar
    
This fat jar should be uploaded and run on hadoop cluster without needing any additional libraries.

# Problems & Solutions

## Problem 1:


1. Create the table by importing the file 'db_scripts/spans.sql'. 
If you have already have the table, skip this step.
2. Run the hadoop task using the command (run it in YARN mode)

        hadoop jar build/libs/cloud-computing-mapreduce-fat.jar pt.ulisboa.tecnico.kkn.toolrunner.BirdMaxSpanToolRunner birds/logs

    With `bird/logs` is the log directory on hdfs
        
3. Wait for the task to finish

## Problem 2: Given a date and a tower-id print the total added estimated
weight of all the birds seen by a tower.

A MapReduce task is run to summarize the weight of the birds spotted by a certain
tower on a certain date. The tower's ID, the date and the summarized weight of all
 the birds is stored in the database, in the table 'weight'.

 The MapReduce <key, value> chain is:
 
    <log_line> -> 
    Mapper -> <"tower_id;date","weight"> -> 
    Combiner -> <"tower_id;date","weight"> ->
    Reducer -> (to database) <"tower_id", "date", "weight">

1. Create the table by importing the file 'db_scripts/weights.sql'. 
If you have already have the table, skip this step.
2. Run the hadoop task using the command (run it in YARN mode)

        hadoop jar build/libs/cloud-computing-mapreduce-fat.jar pt.ulisboa.tecnico.kkn.toolrunner.BirdWeightToolRunner birds/logs

    With `bird/logs` is the log directory on hdfs
        
3. Wait for the task to finish

## Problem 3: Find the birds which haven't appeared in the last week.
We run a Map-Reduce task that find the last appearance of each birds and 
store it in table `appearance` in the database.

### Key-value pair
    <object, log line> 
    -> Mapper -> <bird_tag, time_of_appearance> 
    -> Combiner -> <bird_tag, time_of_last_appearance>
    -> Reducer -> <bird_tag, time_of_last_appearance>

1. Create the table by importing file `db_scripts/appearances.sql`. 
If you have already have the table, skip this step.
2. Run the hadoop task using command

        hadoop jar build/libs/cloud-computing-mapreduce-fat.jar pt.ulisboa.tecnico.kkn.toolrunner.BirdLastRecordToolRunner birds/logs

    With `bird/logs` is the log directory on hdfs

3. Wait for the command to complete

[1]: http://www.gradle.org/
[2]: http://marketplace.eclipse.org/content/gradle-integration-eclipse-44
[3]: http://www.mkyong.com/tutorials/testng-tutorials/
[4]: http://testng.org/doc/documentation-main.html
